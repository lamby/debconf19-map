# lamby's DebConf19 map

My personal map of interesting things in/around DebConf19.

  https://lamby.pages.debian.net/debconf19-map/

## Contribute

To add/suggest your own items, please edit the `_data/locations.yml` file:

  https://salsa.debian.org/lamby/debconf19-map/edit/master/_data/locations.yml

... and/or create a new merge request:

  https://salsa.debian.org/lamby/debconf19-map/merge_requests

## License

This map is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
